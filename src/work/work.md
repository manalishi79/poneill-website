---
title: Work
layout: internal.njk
---


## Work
<div class="pon-reading-wrapper">
<p class="pon-text-lead text-muted">A selection of services, products and design projects I've worked on.</p>

<br>
<br>

<h3>Assessing risks with released prisoners</h3>
<p class="pon-caption text-muted">
	His Majesty's Prison and Probation Service
</p>
<p>
	Leading design of a risk assessment tool which drew data from
	various systems to give probation officers a holistic picture of a
	newly released prisoner. A complex tool I guided design and user
	experience on through Private Beta creating one stop shop for
	assessing risky cases.
</p>
<p>Contact me to request this case study.</p>
<br>
<br>

### Immigration and Visas
<p class="pon-caption text-muted">UK Government</p>

10 month project looking to consolidate the user experience for an account model. Working in a complex multi-year programme to re-imagine the electronic visa system, this project encompassed marrying <a href="https://www.youtube.com/watch?v=uDSnJ2-iG3E">passport biometric identity verification app</a> with proving services for visitors to the UK.

Contact me to request this case study.

<br>
<br>

### Football Policing
<p class="pon-caption text-muted">Home Office</p>

Product development and design of caseworking system for <a href="https://www.gov.uk/government/groups/united-kingdom-football-policing-unit-ukfpu
">Football Policing.</a> In a multidiscplinary team I took this product through discovery to alpha developing user journeys, prototypes and finished designs for the core user group of in-stadium football police officers.

Contact me to request this case study.

<br>
<br>

### Adviser Portal 
<p class="pon-caption text-muted">Citizens Advice</p>

Redesign of the sectional navigation and information structure on this key <a href="https://www.citizensadvice.org.uk/about-us/adviser-resources/advisernet-information-system/">intranet service</a> providing resources and information support for advisers across the Citizens Advice network.

Contact me to request this case study.

<br>
<br>

### Navigating the Financial Times
<p class="pon-caption text-muted">Financial Times</p>

Development of navigation for the Financial Times' redesigned responsive website. Thsi project involved conducting geurilla user testing with diverse user groups and incorporating a deep site information architecture.

</div>

<br>
<br>

