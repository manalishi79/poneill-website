---
title: Contact
layout: internal.njk
---


## Contact
<div class="pon-reading-wrapper">
<p>If your project needs some design consultancy or if you have any questions on anything you've seen here, get in touch.</p>
<p>Email me at pauloneill79 (a) gmail.com</p>
<br>

### Other ways to get in touch

<p>Follow me <a href="https://twitter.com/pauloneill79">@pauloneill79</a></p>

<p>Connect with me on <a href="https://www.linkedin.com/in/paulmoneill/">LinkedIn</a></p>
<br>
</div>