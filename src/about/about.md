---
title: About me
layout: internal.njk
---
## About me
<div class="pon-reading-wrapper">
<p>
16 years design experience, 10 developing creative user-centered solutions in multi-disciplinary agile teams, 5 before that in graphic design and branding for small to medium enterprises and a year in between traveling around the world.
</p>

<p>
I'm a collaborative designer working in the open who takes a <a href="https://designthinking.ideo.com/#design-thinking-in-context">design thinking approach</a> to problem-solving. Understanding the constraints and context of a business domain forms the basis for all my work. Consolidating those business goals with needs of end-customers ultimately drives the best solutions for all. Remember too, a digital solution is never done. Assessing analytics and user research underpin the evidence I use to iterate designs always seeking to improve usability, purpose and clarity for users.
</p>

<p>
I've used a mix of tools and methods to analyse the problem space within briefs. Most commonly, usability testing blended with <a href="https://www.interaction-design.org/literature/topics/contextual-interviews">contextual enquiry</a> is how I derive a balance of practical fixes for the product along with deep user  insight. Observing your service in real-life and conducting unbiased user interviews is foundational to uncovering user needs and opportunities for business innovation. This is the area I love to facilitate partners with brainstorming workshops and
developing prototypes.
</p>

### What is a prototype? 

<p>When asked we intrinsically feel we understand what a prototype is. It's usual some kind of minimal scaled down version of a potential final solution. Most people tend to think of examples from the world of engineering, model test airplanes for example. Those in tech may think of the plywood assembled Apple 1 prototype from Steve Jobs.</p> 

<p>But effective design also utilises prototypes to validate your ideas before scaling up the solution. The order and choice of questions you put on a form can be validated quickly with user testing. A new product idea or new feature can be tested with a <a href="https://www.interaction-design.org/literature/article/a-simple-introduction-to-lean-ux">Lean UX prototype.</a> Even new ways-of-working or changes to company operational patterns can prototyped. 
</p>
<br>
<br>
</div>

