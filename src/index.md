---
title: Home page
layout: index.njk
---

<div class="pon-hero-home-container clearfix">
<div class="pon-hero-home-image">
<img src="./images/man-posting.jpg" height=100% width=100% alt="man posting on a wall" >
</div>

<div class="pon-hero-home-blurb">
    <h1><span class="pon-lead-header">Paul O’Neill</span>
    <br>User experience designer</h1>
<br>
<p class="pon-text-lead text-muted">Innovation design driven by understanding users needs to create real customer value.</p>
</div>

</div>

